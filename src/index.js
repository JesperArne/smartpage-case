import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from "./serviceWorker";
import {
  Navigation,
  Home,
  Footer,
  ProduktListe,
  Kurv
} from "./components";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


ReactDOM.render(
  <Router>
    <Navigation />
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/ProduktListe" element={<ProduktListe />}/>
        <Route path="/Kurv" element={<Kurv />}/>
      </Routes>
      <Footer />
  </Router>,
  document.getElementById('root')
);

serviceWorker.unregister()
