import React from "react";
import { NavLink } from "react-router-dom";

function Navigation() {
  return (
    <div className="navigation">
      <nav className="navbar navbar-expand navbar-dark bg-dark"> 
        <div className="container">
          <NavLink className="navbar-brand" to="/">
            Variant
          </NavLink>
          <div>
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <NavLink className="nav-link" to="/ProduktListe">
                  Produkter
                  <span className="sr-only">(current)</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/Kurv">
                  Kurv
                </NavLink>
              </li>
              {/* <li className="nav-item">
                <NavLink className="nav-link" to="/contact">
                  Contact
                </NavLink>
              </li> */}
              {/* <li className="nav-item">
                <NavLink className="nav-link" to="/blog">
                  Kurv
                </NavLink> 
              </li>*/}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navigation;