import React, { Component } from "react";
import { AddProducts } from "./services/AddProducts";


function ProduktListe() {
  return (

          <div class="col-lg-5">
            <h1 class="font-weight-light">Produkter</h1>
            <p>
              Oversigt af produkter
            </p>
            <div ClassName="App">
              <AddProducts/>
            </div>
          </div>
  );
}

export default ProduktListe;
