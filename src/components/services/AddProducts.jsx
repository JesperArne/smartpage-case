import React from "react";
import { Card, CardGroup} from "react-bootstrap";

class AddProducts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" }, // try ”application/x-www-form-urlencoded”, if not working
    };
    fetch(
      "http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/products?RepositoryName=products&QueryName=products",
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => this.setState({ products: data.Products }));
  }



  render() {
    const { products } = this.state;
    console.log(products.Id);
    return (
      <CardGroup style={{ width: '150em' }}>
        {products.map((product) => (
          <Card >
            <Card.Img variant="top" src="holder.js/100px160" />
            <Card.Body>
              <Card.Title>{product.Name}</Card.Title>
              <Card.Text>{product.ShortDescription}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <p>{product.Price.PriceFormatted}</p>
              <button onClick={AddToBasket(product.Id)}>Læg i Kurv</button>
            </Card.Footer>
          </Card>
        ))}
      </CardGroup>
    );
  }
}

function    AddToBasket(Id){
    //Emit specific  id so it can be used in the basket
    console.log(Id);
}


export { AddProducts };
